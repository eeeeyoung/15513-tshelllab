/*
 * name: Yang YI
 * andrewid: yangyi
 */

/* 
 * tsh - A tiny shell program with job control with I/O redirection (< and >).
 *       Jobs can be run in the foreground and, if the command lines ends with
 *       an &, in the background as well. The command line typed by the user
 *       should consist of a name and zero or more arguments, all separated by
 *       one or more spaces. tsh executes built-in command in the shell process
 *       but fork off a child and run in the context of the child process. 
 *       All zombies children are reaped.
 */

#include "csapp.h"
#include "tsh_helper.h"

#include <sys/wait.h>
#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/*
 * If DEBUG is defined, enable contracts and printing on dbg_printf.
 */
#ifdef DEBUG
/* When debugging is enabled, these form aliases to useful functions */
#define dbg_printf(...) printf(__VA_ARGS__)
#define dbg_requires(...) assert(__VA_ARGS__)
#define dbg_assert(...) assert(__VA_ARGS__)
#define dbg_ensures(...) assert(__VA_ARGS__)
#else
/* When debugging is disabled, no code gets generated for these */
#define dbg_printf(...)
#define dbg_requires(...)
#define dbg_assert(...)
#define dbg_ensures(...)
#endif

/* Function prototypes */
void eval(const char *cmdline);

void sigchld_handler(int sig);
void sigtstp_handler(int sig);
void sigint_handler(int sig);
void sigquit_handler(int sig);
void cleanup(void);

/* List of Helper Functions */
int is_builtin_cmd(struct cmdline_tokens tokens);
void handle_bg_fg(struct cmdline_tokens tokens);
int handle_file_opening(char * filename, int in_or_out);

/* volatile sig_atomic_t flag to indicate whether foreground job is finshed */
volatile sig_atomic_t fg_flag;

/* 
 * main:  Entry function to the shell and sets up the working environment. 
 *        Parses cmdline arguments, creates env variables, initializes job,
 *        lists, installing signal handlers and emitting the shell prompt, and
 *        evaluating and executing the input.
 * 
 * argc:  Number of input arguments
 * argv:  ptr to the list of input char * arguments
 * 
 * return: Never returns, program only exits via SIGQUIT call.
 * 
 * Precondition:  No preconditions.
 * Error conditions:  The function may fail to perform I/O redirection or clean
 *                    up job lists in case of abnormal program termination.
 */
int main(int argc, char **argv) {
    char c;
    char cmdline[MAXLINE_TSH];  // Cmdline for fgets
    bool emit_prompt = true;    // Emit prompt (default)

    // Redirect stderr to stdout (so that driver will get all output
    // on the pipe connected to stdout)
    if (dup2(STDOUT_FILENO, STDERR_FILENO) < 0) {
        perror("dup2 error");
        exit(1);
    }

    // Parse the command line
    while ((c = getopt(argc, argv, "hvp")) != EOF) {
        switch (c) {
        case 'h':                   // Prints help message
            usage();
            break;
        case 'v':                   // Emits additional diagnostic info
            verbose = true;
            break;
        case 'p':                   // Disables prompt printing
            emit_prompt = false;
            break;
        default:
            usage();
        }
    }

    // Create environment variable
    if (putenv("MY_ENV=42") < 0) {
        perror("putenv");
        exit(1);
    }

    // Set buffering mode of stdout to line buffering.
    // This prevents lines from being printed in the wrocommandng order.
    if (setvbuf(stdout, NULL, _IOLBF, 0) < 0) {
        perror("setvbuf");
        exit(1);
    }

    // Initialize the job list
    init_job_list();

    // Register a function to clean up the job list on program termination.
    // The function may not run in the case of abnormal termination (e.g. when
    // using exit or terminating due to a signal handler), so in those cases,
    // we trust that the OS will clean up any remaining resources.
    if (atexit(cleanup) < 0) {
        perror("atexit");
        exit(1);
    }

    // Install the signal handlers
    Signal(SIGINT,  sigint_handler);   // Handles Ctrl-C
    Signal(SIGTSTP, sigtstp_handler);  // Handles Ctrl-Z
    Signal(SIGCHLD, sigchld_handler);  // Handles terminated or stopped child

    Signal(SIGTTIN, SIG_IGN);
    Signal(SIGTTOU, SIG_IGN);

    Signal(SIGQUIT, sigquit_handler);

    // Execute the shell's read/eval loop
    while (true) {
        if (emit_prompt) {
            printf("%s", prompt);

            // We must flush stdout since we are not printing a full line.
            fflush(stdout);
        }

        if ((fgets(cmdline, MAXLINE_TSH, stdin) == NULL) && ferror(stdin)) {
            perror("fgets error");
            exit(1);
        }

        if (feof(stdin)) {
            // End of file (Ctrl-D)
            printf("\n");
            return 0;
        }

        // Remove any trailing newline
        char *newline = strchr(cmdline, '\n');
        if (newline != NULL) {
            *newline = '\0';
        }

        // Evaluate the command line
        eval(cmdline);
    }

    return -1; // control never reaches here
}

/*
 * eval: Evaluate the command line from user input with I/O redirection.
 *       If user requests a built-in command (quit, jobs, bg, fg) then execute
 *       it in the shell process. If not, fork a child process and run the
 *       job in the context of the child. If a foreground job is requested,
 *       the shell waits for the child process to exit by monitoring fg_flag.
 * 
 * cmdline: the command line from user input.
 * 
 * @return: NULL
 * 
 * @Conditions: Signals must be blocked, job list be previously initialized.
 * @Error:      The function may fail for errors related to file openings
 *              and execve exceptions.
 *
 * NOTE: The shell is supposed to be a long-running process, so this function
 *       (and its helpers) should avoid exiting on error.  This is not to say
 *       they shouldn't detect and print (or otherwise handle) errors!
 */
void eval(const char *cmdline) {
    parseline_return parse_result;
    struct cmdline_tokens token;
    pid_t pid = 0;
    jid_t job_id;
    int out_fd = 1, in_fd = 0;
    sigset_t mask_all, mask_one, prev_mask, empty_mask;
    sigemptyset(&empty_mask);
    sigemptyset(&mask_one);
    sigfillset(&mask_all);
    sigaddset(&mask_one, SIGCHLD);

    // Parse command line
    parse_result = parseline(cmdline, &token);
    if (parse_result == PARSELINE_ERROR || parse_result == PARSELINE_EMPTY) {
        return;
    }
    if ((in_fd = handle_file_opening(token.infile, 0)) < 0) return;
    if ((out_fd = handle_file_opening(token.outfile, 1)) < 0) return;

    // Handles built-in commands
    if (!is_builtin_cmd(token)) {

        // blocks SIGCHILD in both parent and child
        sigprocmask(SIG_BLOCK, &mask_all, &prev_mask);
        if ((pid = fork()) == 0) {

            // Use current process ID as its process group ID
            setpgid(0, 0);
            sigprocmask(SIG_SETMASK, &prev_mask, NULL);
            /* I/O redirection */
            if (in_fd != 0) {
                dup2(in_fd, 0);
                close(in_fd);
            }
            if (out_fd != 1) {
                dup2(out_fd, 1);
                close(out_fd);
            }
            if (execve(token.argv[0], token.argv, environ) < 0) {
                sio_printf("failed to execute: %s\n", token.argv[0]);
                exit(0);
            }
        }
        
        // Returns 1 if job runs on background; 0 if job runs on foreground
        add_job(pid, parse_result ? BG : FG, cmdline);
        job_id = job_from_pid(pid);
        
        // foreground, waits for volatile fg_flag to be set by SIGCHLD handler
        if (!parse_result) {
            fg_flag = 0;
            while (!fg_flag) {
                sigsuspend(&empty_mask);
            }
        }
        else {
            // background, doesn't wait and print directly
            sio_printf("[%d] (%d) %s\n", job_id, pid, cmdline);
        }
        sigprocmask(SIG_SETMASK, &prev_mask, NULL);
    }
    return;
}

/*****************
 * Signal handlers
 *****************/

/*
 * sigchld_handler: Signal Handler for handling a caught SIGCHLD signal sent by
 *                  the kernel whenver a child job is killed and becomes a
 *                  zombie. The handler reaps all available zombies in a while
 *                  loop, uses WNOHANG and WUNTRACED to not wait for currently
 *                  running children and return stopped status of children.
 * 
 * @input sig:      SIGCHLD signal (17)
 * 
 * @return:         NULL
 * 
 * @Precondition:   Signals must be blocked. Errno must be restored.
 *                  A job list must be previously initialized.
 */
void sigchld_handler(int sig) {
    int olderrno = errno;
    int chld_state;
    sigset_t mask_all, prev_all;
    jid_t fg_job_id, job_id;
    pid_t pid, pid_fg;
    sigfillset(&mask_all);

    while ((pid = waitpid(-1, &chld_state, WNOHANG|WUNTRACED)) > 0) {
        sigprocmask(SIG_BLOCK, &mask_all, &prev_all);
        if ((fg_job_id = fg_job()) > 0) {
            pid_fg = job_get_pid(fg_job_id);
            if (pid_fg == pid) fg_flag = 1;
        } else {
            fg_flag = 1;
        }
        /* child normal exit */
        if (WIFEXITED(chld_state)) {
            job_id = job_from_pid(pid);
            delete_job(job_id);
        }
        /* child stopped */
        else if (WIFSTOPPED(chld_state)) {
            job_id = job_from_pid(pid);
            if (job_id > 0 && job_get_state(job_id) != ST) {
                sio_printf("Job [%d] (%d) stopped by signal %d\n",
                        job_id, pid, WSTOPSIG(chld_state));
                job_set_state(job_id, ST);  // does not remove from job list
            }
        }
        /* child interrupted */
        else if (WIFSIGNALED(chld_state)) {
            job_id = job_from_pid(pid);
            if (job_id > 0) {
                sio_printf("Job [%d] (%d) terminated by signal %d\n",
                            job_id, pid, WTERMSIG(chld_state));
                delete_job(job_id);
            }
        }
        sigprocmask(SIG_SETMASK, &prev_all, NULL);
    }
    errno = olderrno;
    return;
}

/*
 * sigint_handler: Signal Handler for handling a caught SIGINT signal.
 *                 Retrieves the foreground job ID from the job list, obtains
 *                 its process ID, then sends SIGINT to the foreground process
 *                 ground.
 * 
 *                 If no foreground job is present, this signal handler does
 *                 not emit the kill command. 
 * 
 * @input sig:     SIGINT signal (2)
 * 
 * @return:        NULL
 * 
 * @Precondition:  Signals must be blocked. Errno must be restored.
 *                 A job list must be previously initialized.
 * 
 * @Error Condition: The function may fail to kill a process group by sending
 *                   it SIGINT if the process group no longer exists.
 */
void sigint_handler(int sig) {
    int olderrno = errno;
    sigset_t mask_all, prev_mask;
    jid_t job_id;
    sigfillset(&mask_all);

    sigprocmask(SIG_BLOCK, &mask_all, &prev_mask);
    if ((job_id = fg_job()) > 0) {
        pid_t pid = job_get_pid(job_id);
        if (kill(-pid, SIGINT) < 0) {
            //
        }
    }
    sigprocmask(SIG_SETMASK, &prev_mask, NULL);
    errno = olderrno;
    return;
}

/*
 * sigtstp_handler: Signal Handler for handling a caught SIGTSTP signal.
 *                  Retrieves the foreground job ID from the job list, obtains
 *                  its process ID, then sends SIGTSTP to the foreground 
 *                  process ground.
 *                  If no foreground job is present, this signal handler does
 *                  not emit the kill command. 
 * 
 * sig:             SIGTSTP signal (20)
 * 
 * @return:         NULL
 * 
 * @Precondition:   Signals must be blocked. Errno must be restored.
 *                  A job list must be previously initialized.
 * 
 * @Error Condition: The function may kill to stop a process group by sending
 *                   it SIGTSTP if the process group no longer exists.
 */
void sigtstp_handler(int sig) {
    int olderrno = errno;
    sigset_t mask_all, prev_mask;
    sigfillset(&mask_all);
    jid_t job_id;

    sigprocmask(SIG_BLOCK, &mask_all, &prev_mask);
    if ((job_id = fg_job()) > 0) {
        pid_t pid = job_get_pid(job_id);    
        if (kill(-pid, SIGTSTP) < 0) {
            //
        }
    }
    sigprocmask(SIG_SETMASK, &prev_mask, NULL);
    errno = olderrno;
    return;
}

/*
 * cleanup - Attempt to clean up global resources when the program exits. In
 * particular, the job list must be freed at this time, since it may contain
 * leftover buffers from existing or even deleted jobs.
 */
void cleanup(void) {
    // Signals handlers need to be removed before destroying the joblist
    Signal(SIGINT,  SIG_DFL);  // Handles Ctrl-C
    Signal(SIGTSTP, SIG_DFL);  // Handles Ctrl-Z
    Signal(SIGCHLD, SIG_DFL);  // Handles terminated or stopped child

    destroy_job_list();
}


/*****************
 * Helper Functions
 *****************/

/*
 * handle_file_opening: Helper function for opening files for reading from and
 *                      writing to given the path to the file. If an exception
 *                      occurred, print out error message and return -1.
 * 
 *                      If a file to read: use read-only.
 *                      If a file to write: use write-only, create if absent
 *                      and deleles and overwrites existing files entirely.
 * 
 * @input sig:          filename:  the input path of the file to open
 *                      in_or_out: indicates whether the file is a file to read
 *                                 from or a file to write to
 * 
 * @return:             fd: file descriptor number, -1 if error.
 * 
 * @Precondition:       Errno must be restored.
 * 
 * @Error Cond:         The function may fail to create valid file descriptor
 *                      for EACCES(13): Permission denied, ENOENT(2): No such 
 *                      file or directory, or any other unhandled exceptions.
 */
int handle_file_opening(char * filename, int in_or_out)
{
    int fd = in_or_out;
    int olderrno = errno;

    if (filename != NULL) {
        if (in_or_out == 0) {  /* If is a read file */
            fd = open(filename, O_RDONLY, 0);
        } else {               /* If is a write file */
            fd = open(filename, O_WRONLY|O_CREAT|O_TRUNC, 0666);
        }
        if (fd < 0) {
            switch(errno) {
                case EACCES:
                    sio_printf("%s: Permission denied\n", filename);
                    break;
                case ENOENT:
                    sio_printf("%s: No such file or directory\n", filename);
                    break;
                default:
                    sio_printf("Open file error\n");
                    break;
            }
        }
    }

    errno = olderrno;
    return fd;
}

/*
 * is_builtin_cmd: Helper function for handling cmd input if is a builtin cmd.
 *                 Handles empty input, quit, jobs, fg and bg. Uses a switch
 *                 statement to jump to different commands.
 * 
 * tokens:         struct cmdline_tokens set by parseline to extract info from.
 * 
 * @return:        1 if builtin command, 0 otherwise.
 * 
 * @Preconditions: Signals must be blocked for "jobs" cmd.
 * 
 * @Error Cond:    The function may fail to list jobs if an error occurred
 *                 while writing to the file descriptor.
 */
int is_builtin_cmd(struct cmdline_tokens tokens)
{
    sigset_t mask_all, prev_mask;
    int out_fd = 1;
    switch(tokens.builtin) 
    {
        case BUILTIN_NONE:
            return 0;
        
        case BUILTIN_QUIT:
            exit(0);
        
        case BUILTIN_JOBS:
            sigfillset(&mask_all);
            sigprocmask(SIG_BLOCK, &mask_all, &prev_mask);

            out_fd = handle_file_opening(tokens.outfile, 1);
            if (!list_jobs(out_fd)) {
                sio_printf("Write to file error");
            };
            if (tokens.outfile != NULL) close(out_fd);

            sigprocmask(SIG_SETMASK, &prev_mask, NULL);
            return 1;
        
        case BUILTIN_FG:
        case BUILTIN_BG:
            handle_bg_fg(tokens);
            return 1;
    }
    return 0;
}

/*
 * handle_bg_fg: Handles fg and bg commands. Restarts job specified by PID or
 *               JID by sending it a SIGCONT signal and run it either in
 *               foreground or background.
 * 
 * tokens:       struct cmdline_tokens set by parseline to extract info from.
 * 
 * @return:      NULL
 *
 * @Preconditions: A valid job list must has been previously initialized. 
 *                 Signals must be blocked.
 * 
 * @Error Conditions: The function will if the requested job no longer exists,
 *                    command line input is empty or does not contain a valid
 *                    job id or process id.
 */
void handle_bg_fg(struct cmdline_tokens tokens)
{
    pid_t pid;
    jid_t jid;
    sigset_t mask_all, prev_mask, empty_mask;
    sigemptyset(&empty_mask);
    sigfillset(&mask_all);
    
    // checks for empty job_id or process id
    if (tokens.argv[1] == NULL) {
        sio_printf("%s command requires PID or %%jobid argument\n",
                    tokens.builtin == BUILTIN_FG ? "fg" : "bg");
        return;
    }
    // checks for job_id or process id validity (digits)
    for(int i = 0; i < strlen(tokens.argv[1]); i++) {
        if (!isdigit(*(tokens.argv[1] + i)) &&
            *(tokens.argv[1] + i) != '%') {
                sio_printf("%s: argument must be a PID or %%jobid\n",
                    tokens.builtin == BUILTIN_FG ? "fg" : "bg");
                return;
            }
    }
    char *input_number = tokens.argv[1];
    sigprocmask(SIG_BLOCK, &mask_all, &prev_mask);
    if (*(input_number) == '%') {
        if (sscanf(tokens.argv[1], "%%%d", &jid) > 0) {
            if (job_exists(jid) && job_get_state(jid) != UNDEF) {
                pid = job_get_pid(jid);
            } else {
                sio_printf("%%%d: No such job\n", jid);
                sigprocmask(SIG_SETMASK, &prev_mask, NULL);
                return;
            }
        }
    } else {
        if (sscanf(tokens.argv[1], "%d", &pid) > 0) {
            if ((jid = job_from_pid(pid)) > 0 &&
                 job_exists(jid) &&
                 job_get_state(jid) != UNDEF) {
            } else {
                sio_printf("(%d): No such process\n", pid);
                sigprocmask(SIG_SETMASK, &prev_mask, NULL);
                return;
            }
        }
    }

    if (tokens.builtin == BUILTIN_BG) {
        if (job_get_state(jid) == BG) {
            sio_printf("[%d] (%d) %s\n", jid, pid, job_get_cmdline(jid));
        } else if (job_get_state(jid) == ST) {
            job_set_state(jid, BG);
            sio_printf("[%d] (%d) %s\n", jid, pid, job_get_cmdline(jid));
            kill(-pid, SIGCONT);
            sigprocmask(SIG_SETMASK, &prev_mask, NULL);
        }
    }

    if (tokens.builtin == BUILTIN_FG) {
        fg_flag = 0;
        if (job_exists(jid)) {
            job_set_state(jid, FG);
            kill(-pid, SIGCONT);
            while (!fg_flag)
            {
                sigsuspend(&empty_mask);
            }
            sigprocmask(SIG_SETMASK, &empty_mask, NULL);
        }
    }
    sigprocmask(SIG_SETMASK, &prev_mask, NULL);
    return;
}